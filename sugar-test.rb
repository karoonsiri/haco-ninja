#!/usr/bin/env ruby

require 'rubygems'
require 'rest_client'
require 'json'
require 'digest/md5'
require 'pp'

user = "swiftlet"
pass = "#EDC4rfv"

def run_request(method, params)
  post_param = {
    "method" => method,
    "input_type" => "JSON",
    "response_type" => "JSON",
    "rest_data" => params.to_json
  }
  
  url = "http://localhost/sugarcrm/service/v4_1/rest.php"

  headers = {
    "User-Agent" => "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0",
  }

  resource = RestClient::Resource.new(url)
  resource.post(post_param, headers)
end

def login(user, password)
  login_params = {
    "user_auth" => {
      "user_name" => user,
      "password" => Digest::MD5.hexdigest(password),
      "version" => 1
    },
    "application_name" => "RestTest",
    "name_value_list" => []
  }
  response = run_request('login', login_params)
  JSON.parse(response)
end

def logout(user)
  logout_params = {
    "session" => user['id']
  }
  run_request('logout', logout_params)
end


user = login(user, pass)
# user = JSON.parse(response.response_body)
# pp user
# exit()

query_params = {
  # Required
  "session" => user['id'],
  "module_name" => "Accounts",
  "query" => "",
  "order_by" => "",
  "offset" => "0",

  # Optional
  "select_fields" => ["id","name"],

  "link_name_to_fields_array" => [
    {
      'name' => 'contacts',
      'value' => ['id','first_name','last_name'],
    },
  ],
  "deleted" => "0",
  "max_results" => "2",
}

relate_params = {
  "session" => user['id'],
  "module_name" => "Accounts",
  "fields" => [],
}

get_modules_params = {
  'session' => user['id'],
  'filter'  => 'all',
}

# response = run_request('get_entry_list', query_params)
# response = run_request('get_module_fields', relate_params)
response = run_request('get_available_modules', get_modules_params)

raw_list = JSON.parse(response)
a = raw_list['modules'].inject([]) do |result, name|
  result << name['module_key']
end
pp a
# ss = raw_list['link_fields'].inject([]) do |result, (key,value)|
#   result << key
# end
# pp ss
exit()


create_params = {
  "session" => user['id'],
  # "session" => "c82b22mobncata5gjf6uqnh3o0",
  "module_name" => "Accounts",
  "name_value_list" => [
    {
      name: "name",
      value: "P Fern2",
    },
    {
      name: "website",
      value: "http://www.example.com",
    },
    {
      name: "email1",
      value: "fern@email.com",
    },
    {
      name: "email2",
      value: "fern2@email.com",
    },
    {
      name: "email3",
      value: "fern3@email.com",
    },
    {
      name: "email4",
      value: "fern4@email.com"
    }
  ]
}

puts JSON.parse(run_request('set_entries', create_params)).pretty_inspect


logout(user)
