require 'spec_helper'
require 'pp'

describe Sugar::Connection do
  context 'Get connection to visual box' do
    it 'Connection to Sugar' do
      a = Sugar::Connection.instance
      a.path = 'sugarce'
      a.host = '192.168.1.16'
      a.user = 'admin'
      a.password = 'adminadmin'

      a.should be_a(Sugar::Connection)
      a.url.should  == 'http://192.168.1.16/sugarce/service/v4_1/rest.php'
    end
  end

  context 'Get connection to visual box' do
    it 'Connection to Sugar' do
      a = Sugar::Connection.instance
      a.path = 'sugarce'
      a.host = '192.168.1.16'
      a.user = 'admin'
      a.password = 'adminadmin'

      a.should be_a(Sugar::Connection)
      a.url.should  == 'http://192.168.1.16/sugarce/service/v4_1/rest.php'
    end
  end
end

describe :create_session do
   it 'Log in to Sugar' do
      a = Sugar::Connection.instance
      a.path = 'sugarce'
      a.host = '192.168.1.16'
      a.user = 'admin'
      a.password = 'adminadmin'

      a.instance_variable_set(:@session_id, nil)
      a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
      end
    end

    it 'Login to Sugar by wrong username' do
      a = Sugar::Connection.instance
      a.path = 'sugarce'
      a.host = '192.168.1.16'
      a.user = 'fern'
      a.password = 'adminadmin'

      a.instance_variable_set(:@session_id, nil)
      expect {a.create_session}.to raise_error('LoginError')
    end

    it 'Login to Sugar by wrong password' do
      a = Sugar::Connection.instance
      a.path = 'sugarce'
      a.host = '192.168.1.16'
      a.user = 'admin'
      a.password = '1111111'

      a.instance_variable_set(:@session_id, nil)
      expect {a.create_session}.to raise_error('LoginError')
    end

    it 'Login to Sugar by wrong path' do
      a = Sugar::Connection.instance
      a.path = 'sugarcrm'
      a.host = '192.168.1.16'
      a.user = 'admin'
      a.password = '1111111'

      a.instance_variable_set(:@session_id, nil)
      expect {a.create_session}.to raise_error('LoginError')
    end
end

describe Sugar::Account do
  before(:each) do
    a = Sugar::Connection.instance
    a.path = 'sugarce'
    a.host = '192.168.1.16'
    a.user = 'admin'
    a.password = 'adminadmin'

    a.instance_variable_set(:@session_id, nil)
    a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
    end
  end

  describe :create , :type => :feature do
    it 'Create a new account' do
       a = Sugar::Account.create(
        'name'=>'fern', 'phone_office'=>'02157855',  'phone_fax'=>'02685588',
        'website'=>'http://www.swiftlet.co.th','employees'=>'20','annual_revenue'=>'10,000,000',
        'email1'=>'chanisas@swiftlet.co.th', 'billing_address_street'=>'piboon',
        'billing_address_city'=>'nontaburi','billing_address_country'=>'bangkok',
        'billing_address_postalcode'=>'10900','description'=>'Test Test account',
        'shipping_address_street'=>'260 moo 9','shipping_address_city'=>'khengkhoi',
        'shipping_address_country'=>'saraburi','shipping_address_postalcode'=>'10800'
      )
      a.name.should == 'fern'
      a.name = 'swiftlet'
      a.save
      a.name.should == 'swiftlet'
      # name is not unique
      a.delete
    end
  end
end

describe Sugar::Contact do
  before(:each) do
    a = Sugar::Connection.instance
    a.path = 'sugarce'
    a.host = '192.168.1.16'
    a.user = 'admin'
    a.password = 'adminadmin'

    a.instance_variable_set(:@session_id, nil)
    a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
    end
  end

  describe :create , :type => :feature do
    it 'Create a new contact' do
      a = Sugar::Contact.create(
        'first_name'=>'chanisas',  'last_name'=>'suwannarang',
        'title'=>'s',              'department'=>'QA',
        'phone_work'=>'02157855',  'phone_mobile'=>'0888586368',
        'email1'=>'chanisas@swiftlet.co.th', 'primary_address_street'=>'piboon',
        'primary_address_city'=>'nontaburi','primary_address_country'=>'bangkok',
        'primary_address_postalcode'=>'10900','description'=>'Test Test',
        'alt_address_street'=>'260 moo 9','alt_address_city'=>'khengkhoi',
        'alt_address_country'=>'saraburi','alt_address_postalcode'=>'10800'
      )
      a.first_name.should == 'chanisas'
      a.first_name = 'chanisa'
      a.save
      a.first_name.should == 'chanisa'
      a.delete
    end
  end
end

describe Sugar::Lead do
  before(:each) do
    a = Sugar::Connection.instance
    a.path = 'sugarce'
    a.host = '192.168.1.16'
    a.user = 'admin'
    a.password = 'adminadmin'

    a.instance_variable_set(:@session_id, nil)
    a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
    end
  end

  describe :create , :type => :feature do
    it 'Create a new lead' do
      a = Sugar::Lead.create(
        'first_name'=>'kitta',  'last_name'=>'suksirisak',
        'title'=>'P',              'department'=>'Engineering',
        'phone_work'=>'02157855',  'phone_mobile'=>'0888586368',
        'email1'=>'kittanai@hotmail.com', 'primary_address_street'=>'piboon',
        'primary_address_city'=>'nontaburi','primary_address_country'=>'bangkok',
        'primary_address_postalcode'=>'10900','description'=>'Test Test',
        'alt_address_street'=>'260 moo 9','alt_address_city'=>'khengkhoi',
        'alt_address_country'=>'saraburi','alt_address_postalcode'=>'10800',
        'account_name'=>'Boiler Thailand co,.Ltd','account_description'=>'sale boiler in Thailand'
      )
      a.first_name.should == 'kitta'
      a.first_name = 'kittanai'
      a.save
      a.first_name.should == 'kittanai'
      a.delete
    end
  end
end

describe Sugar::Case do
  before(:each) do
    a = Sugar::Connection.instance
    a.path = 'sugarce'
    a.host = '192.168.1.16'
    a.user = 'admin'
    a.password = 'adminadmin'

    a.instance_variable_set(:@session_id, nil)
    a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
    end
  end

  describe :create , :type => :feature do
    it 'Create a new case' do
      a = Sugar::Case.create(
        'name'=>'Support Contact','description'=>'connect test test',
        'resolution'=>'Fixed case'
      )
      a.name.should == 'Support Contact'
      a.name = 'Fixed Case'
      a.save
      a.name.should == 'Fixed Case'
      a.delete
    end
  end
end

describe Sugar::Campaign do
  before(:each) do
    a = Sugar::Connection.instance
    a.path = 'sugarce'
    a.host = '192.168.1.16'
    a.user = 'admin'
    a.password = 'adminadmin'

    a.instance_variable_set(:@session_id, nil)
    a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
    end
  end

  describe :create , :type => :feature do
    it 'Create a new campaign' do
      a = Sugar::Campaign.create(
        'name'=>'Campaign','content'=>'connect test test',
        'objective'=>'Target is get account',
        'start_date'=>'2013-08-01', 'end_date'=>'2013-08-07'
      )
      a.name.should == 'Campaign'
      a.name = 'Get account'
      pp a.save
      a.name.should == 'Get account'
      a.delete
    end
  end
end

describe Sugar::Call do
  before(:each) do
    a = Sugar::Connection.instance
    a.path = 'sugarce'
    a.host = '192.168.1.16'
    a.user = 'admin'
    a.password = 'adminadmin'

    a.instance_variable_set(:@session_id, nil)
    a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
    end
  end

  describe :create , :type => :feature do
    it 'Create a new call' do
      a = Sugar::Call.create(
        'name'=>'For meeting','description'=>'test test','duration_minutes'=>'30',
        'date_start'=>'2013-08-06 17:00:00','date_end'=>'2013-08-06 17:30:00'
       )
      a.name.should == 'For meeting'
      a.name = 'send propersal'
      a.save
      a.name.should == 'send propersal'
      a.delete
    end
  end
end


describe Sugar::Meeting do
  before(:each) do
    a = Sugar::Connection.instance
    a.path = 'sugarce'
    a.host = '192.168.1.16'
    a.user = 'admin'
    a.password = 'adminadmin'

    a.instance_variable_set(:@session_id, nil)
    a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
    end
  end

  describe :create , :type => :feature do
    it 'Create a new schedule meeting' do
      a = Sugar::Meeting.create(
        'name'=>'For meeting','description'=>'test test','location'=>'Swiftlet',
        'date_start'=>'2013-08-06 17:00:00','date_end'=>'2013-08-06 17:30:00'
       )
      a.name.should == 'For meeting'
      a.name = 'meeting'
      a.save
      a.name.should == 'meeting'
      a.delete
    end
  end
end

describe Sugar::Task do
  before(:each) do
    a = Sugar::Connection.instance
    a.path = 'sugarce'
    a.host = '192.168.1.16'
    a.user = 'admin'
    a.password = 'adminadmin'

    a.instance_variable_set(:@session_id, nil)
    a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
    end
  end

  describe :create , :type => :feature do
    it 'Create a new task' do
      a = Sugar::Task.create(
        'name'=>'Task','description'=>'test test',
        'date_start'=>'2013-08-06 17:00:00','date_due'=>'2013-08-07 17:00:00'
       )
      a.name.should == 'Task'
      a.name = 'Processing'
      a.save
      a.name.should == 'Processing'
      a.delete
    end
  end
end

describe Sugar::Employees do
  before(:each) do
    a = Sugar::Connection.instance
    a.path = 'sugarce'
    a.host = '192.168.1.16'
    a.user = 'admin'
    a.password = 'adminadmin'

    a.instance_variable_set(:@session_id, nil)
    a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
    end
  end

  describe :create , :type => :feature do
    it 'Create a new employee' do
      a = Sugar::Employee.create( 'user_name'=>'chanisa',
         'first_name'=>'chanisas',
        'last_name'=>'suwannarang'
       )
      a.first_name.should == 'chanisas'
      a.first_name = 'fern'
      a.save
      a.first_name.should == 'fern'
    end
  end
end

describe Sugar::User do
  before(:each) do
    a = Sugar::Connection.instance
    a.path = 'sugarce'
    a.host = '192.168.1.16'
    a.user = 'admin'
    a.password = 'adminadmin'

    a.instance_variable_set(:@session_id, nil)
    a.create_session do |session|
      a.session_id.should_not be_nil
      a.session_id.should_not be_empty
    end
  end

  describe :create , :type => :feature do
    it 'Create a new user' do
      a = Sugar::User.create( 'user_name'=>'chanisa',
         'first_name'=>'chanisas',
        'last_name'=>'suwannarang'
       )
      a.first_name.should == 'chanisas'
      a.first_name = 'fern'
      a.save
      a.first_name.should == 'fern'
    end
  end
end

