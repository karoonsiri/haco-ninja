require 'spec_helper'
require 'sugar/base'

describe Sugar::Base do

  describe :get_entry_list do
    it 'should get records from Account module' do
      subject = Sugar::Base.new
      subject.path = 'sugarce'
      subject.instance_variable_set(:@module_name, 'Accounts')

      subject.create_session do |session|
        params = {
          'deleted' => 0,
        }
        response = subject.get_entry_list(params)

        response.length.should > 1
        response.first.should have_key('id')
        response.first.should have_key('name')
        subject.destroy_session
      end
    end

    it 'should raise NoSessionError' do
      subject = Sugar::Base.new
      subject.path = 'sugarce'
      subject.instance_variable_set(:@module_name, 'Accounts')

      params = {
        'offset' => 0,
        'deleted' => 0,
      }

      expect { subject.get_entry_list(params) }.to raise_error('NoSessionError')
    end

    it 'should limit results to 1 rows' do
      subject = Sugar::Base.new
      subject.path = 'sugarce'
      subject.instance_variable_set(:@module_name, 'Accounts')

      subject.create_session do |session|
        params = {
          'max_results' => 1,
          'deleted' => 0,
        }

        response = subject.get_entry_list(params)
        response.length.should == 1
        subject.destroy_session
      end
    end

    it 'should get only name from a list' do
      subject = Sugar::Base.new
      subject.path = 'sugarce'
      subject.instance_variable_set(:@module_name, 'Accounts')

      subject.create_session do |session|
        params = {
          'select_fields' => ['name'],
          'link_name_to_fields_array' => [
            {
              'name' => 'email_addresses',
              'value' => ['id','email_address'],
            }
          ]
        }

        response = subject.get_entry_list(params)
        response.length.should > 1
        response.first.should have_key('name')
        response.first.should_not have_key('id')
        subject.destroy_session
      end
    end

    it 'should get records with related_fields' do
      subject = Sugar::Base.new
      subject.path = 'sugarce'
      subject.instance_variable_set(:@module_name, 'Accounts')

      subject.create_session do |session|
        params = {
          'select_fields' => ['name'],
          'max_results' => 2,
          'link_name_to_fields_array' => [
            {
              'name' => 'email_addresses',
              'value' => ['id','email_address','opt_out'],
            },
            {
              'name' => 'contacts',
              'value' => ['name']
            }
          ]
        }

        response = subject.get_entry_list(params)
        pp response
        response.length.should > 0
        response.first.should have_key('name')
        response.first.should_not have_key('id')

        response.first['link_list'].should have_key('email_addresses')
        response.first['link_list'].should have_key('contacts')
        # pp response
        subject.destroy_session
      end
    end
  end

end