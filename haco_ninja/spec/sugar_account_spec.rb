require 'spec_helper'
require 'sugar/account'

describe Sugar::Account do

  describe :new do
    it 'must initialize an instance of Account' do
      subject = Sugar::Account.new

      subject.should be_a(Sugar::Account)
    end

    it 'must have module name of Accounts' do
      subject = Sugar::Account.new
      subject.module_name.should == 'Accounts'
    end

    it 'can return set values when called' do
      subject = Sugar::Account.new('name'=>'Karun','website'=>'www.karun.com')
      subject.name.should == 'Karun'
      subject.website.should == 'www.karun.com'
    end

    it "should have access to class' fields if no params passed" do
      subject = Sugar::Account.new
      expect {
        subject.id
        subject.name
        subject.website
      }.to_not raise_error
    end

    it "should be able to set class' fields if no params passed" do
      subject = Sugar::Account.new
      expect {
        subject.name = 'KarunTestParam'
        subject.website = 'www.google.com'
      }.to_not raise_error

      subject.name.should == 'KarunTestParam'
    end

  end

  describe :save do
    it 'should save a new record' do
      subject = Sugar::Account.new('name'=>'Tester')
      subject.save

      subject.name.should == 'Tester'
      subject.id.should_not be_nil
      subject.delete
    end

    it 'should be able to save multiple records' do
      a = Sugar::Account.new('name'=>'A')
      a.save


      b = Sugar::Account.new('name'=>'B')
      b.save

      a.id.should_not == b.id
      a.delete
      b.delete
    end
  end

  describe :update_attributes do
    it 'should update existing records with given attributes' do
      candid = Sugar::Account.create('name'=>'Woramet', 'website'=>'www.ku.ac.th')

      candid.update_attributes('website'=>'www.google.com')

      subject = Sugar::Account.find(candid.id)

      subject.name.should == candid.name
      subject.website.should == 'www.google.com'

      subject.delete
    end
  end

  describe :create do
    it 'should create (new & save) a new record' do
      subject = Sugar::Account.create('name'=>'FernFern')

      subject.id.should_not be_nil
      subject.name.should == 'FernFern'
      subject.delete
    end
  end

  describe :find do
    it 'should find an existing record' do
      candid = Sugar::Account.create(
        'name' => 'Karun@EV:OAIH', 'email1' => 'karun@karun.com',
        'email2' => 'haha@haha.com'
      )

      subject = Sugar::Account.find(candid.id)

      subject.id.should == candid.id
      subject.name.should == candid.name
      subject.delete
    end

    it 'should find nothing' do
      candid = Sugar::Account.find('hsssahahahahah')
      candid.should be_nil
    end
  end

  describe :find_where do
    it 'should find an existing records from given criteria' do
      candid = Sugar::Account.create('name'=>'Alex Roger Karun', 'website'=>'www.google.com')
      subject = Sugar::Account.find_where(
        "accounts.name = 'Alex Roger Karun'",
        "accounts.website = 'www.google.com'"
      )

      subject.first.id.should == candid.id
      subject.first.name.should == candid.name

      candid.delete
    end

    it 'should find nothing' do
      candid = Sugar::Account.find_where("accounts.name = 'qp479q[ije;cnv as;eh   p029h'","accounts.website = 'www.google.com'")
      candid.should == []
    end

    it 'should find multiple records from given criteria' do
      candid = []
      candid << Sugar::Account.create('name'=>'SugarTestaksdjfiu8943uraviausdfi')
      candid << Sugar::Account.create('name'=>'SugarTestwieu897=hjjcvn93451gb[g574203894h')

      subject = Sugar::Account.find_where("accounts.name like '%SugarTest%'")
      subject.length.should == 2

      candid.each {|del| del.delete}
    end

    it 'returns deleted records' do
      candid = []
      candid << Sugar::Account.create('name'=>'SugarTestaksdjfiu8943uraviausdfi')
      candid << Sugar::Account.create('name'=>'SugarTestwieu897=hjjcvn93451gb[g574203894h')

      subject = Sugar::Account.find_where('accounts.deleted = 1')

      subject.each do |focus|
        focus.deleted.should == '1'
      end

      candid.each {|del| del.delete}
    end
  end

  describe :all do
    it 'should get all records from a module' do
      length1 = Sugar::Account.all.length
      candid = []
      candid << Sugar::Account.create('name'=>'Karun Naja')
      candid << Sugar::Account.create('name'=>'Karun Naja2')

      length2 = Sugar::Account.all.length
      length2.should == (length1 + 2)
      candid.each {|del| del.delete}
    end
  end

  describe :delete do
    it 'should delete a record' do
      candid = Sugar::Account.create('name'=>'Bogdan Alin Ota')
      candid.delete

      subject = Sugar::Account.find(candid.id)
      subject.should be_nil
    end
  end

  describe :link_fields do
    it "should return specified link_fields" do
      Sugar::Account.link_fields.should include({
        'name'=>'email_addresses','value'=>['id','email_address']
      })
    end
  end

  describe :associate! do
    it "should associate a bean to another bean" do
      subject = Sugar::Account.create('name'=>'test link')
      link = Sugar::Contact.create('first_name'=>'karun', 'last_name'=>'link')

      subject = subject.associate!(link)
      subject.related_fields.should have_key('contacts')

      # This will loop through contacts
      # subject.related_fields['contacts'].each do |data|
      #   contact = Sugar::Contact.new(contact)
      # end

      link.delete
      subject.delete
    end

    it "should raise SetRelationshipError if owner is not saved" do
      subject = Sugar::Account.new
      link = Sugar::Contact.create('first_name'=>'karun', 'last_name'=>'link')

      expect { subject.associate!(link) }.to raise_error("SetRelationshipError")
      link.delete
    end

    it "should associate a bean with many relationships on many modules" do
      subject = Sugar::Account.create('name'=>'test link')
      link = Sugar::Contact.create('first_name'=>'k', 'last_name'=>'L')
      link2 = Sugar::Opportunity.create('name'=>'test opportunity')

      subject.associate!(link)
      result = subject.associate!(link2)

      result.related_fields.should have_key('contacts')
      result.related_fields.should have_key('opportunities')

      link2.delete
      link.delete
      subject.delete
    end
  end

  describe :disassociate! do
    it "should disassociate a bean from owner bean" do
      subject = Sugar::Account.create('name' => 'linklinklink')
      link = Sugar::Contact.create('first_name' => 'Karun', 'last_name' => 'link')

      result = subject.associate!(link)

      result.related_fields.should have_key('contacts')

      result = subject.disassociate!('contacts', link.id)
      result.related_fields.should_not have_key('contacts')

      link.delete
      subject.delete
    end
  end

  context 'Editing records' do
    it "should change attributes' values" do
      a = Sugar::Account.new('name'=>'KarunSiri','website'=>'www.karun.com')
      a.name.should == 'KarunSiri'
      a.website.should == 'www.karun.com'
      a.save

      # Change value and save here
      a.name = 'FernFern'
      a.save

      b = Sugar::Account.find(a.id)
      b.name.should == 'FernFern'

      a.delete
    end
  end

end
