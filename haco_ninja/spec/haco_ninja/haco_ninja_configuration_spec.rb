require 'spec_helper'

describe HacoNinja::Configuration do
  context 'Default setting' do
    it 'should have default settings set' do
      HacoNinja::Configuration.configuration.reset
      HacoNinja::Configuration.configuration.sugar_host.should == 'localhost'
      HacoNinja::Configuration.configuration.sugar_path.should == 'sugarce'
      HacoNinja::Configuration.configuration.sugar_user.should == 'admin'
      HacoNinja::Configuration.configuration.sugar_password.should == '123456'
      HacoNinja::Configuration.configuration.sugar_session_interval == 4
      HacoNinja::Configuration.configuration.sugar_max_retry == 3
      HacoNinja::Configuration.configuration.sugar_cache == true
    end
  end

  describe :configure do
    it 'should change settings after configure' do
      HacoNinja::Configuration.configuration.reset
      HacoNinja::Configuration.configure do |config|
        config.sugar_host = 'hahaha'
        config.sugar_path = 'hehehe'
        config.sugar_user = 'admin'
        config.sugar_password = 'letmein'
        config.sugar_cache = true
      end

      HacoNinja::Configuration.configuration.sugar_host.should == 'hahaha'
      HacoNinja::Configuration.configuration.sugar_path.should == 'hehehe'
      HacoNinja::Configuration.configuration.sugar_user.should == 'admin'
      HacoNinja::Configuration.configuration.sugar_password.should == 'letmein'
      HacoNinja::Configuration.configuration.sugar_cache == true
    end
  end
end