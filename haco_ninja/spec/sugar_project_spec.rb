require 'spec_helper'

# This spec tests the module with singular module name
# This Project module has module name 'Project' instead of 'Projects'
describe Sugar::Project do
  describe :new do
    it "should instantiate an instance of Project module" do
      expect { Sugar::Project.new }.to_not raise_error
    end
  end
end
