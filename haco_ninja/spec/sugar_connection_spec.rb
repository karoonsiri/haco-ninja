require 'spec_helper'
require 'sugar/connection'

describe Sugar::Connection do
  before(:each) do
    HacoNinja::Configuration.configure do |config|
      config.sugar_host = 'localhost'
      config.sugar_path = 'sugarce'
      config.sugar_user = 'admin'
      config.sugar_password = '123456'
      config.sugar_session_interval = 4
      config.sugar_max_retry = 3
    end
    @connection = Sugar::Connection.clone
  end

  context 'Get connection instance' do
    it 'must return instance of the class' do
      a = Sugar::Connection.instance
      a.should be_a(Sugar::Connection)
    end

    it 'must have only one instance through every call' do
      a = Sugar::Connection.instance
      a.should be_a(Sugar::Connection)

      b = Sugar::Connection.instance
      b.should be_a(Sugar::Connection)

      a.should == b
    end
  end
 
  context 'test instance variable' do
    it 'should have appropriate settings' do
      a = @connection.instance

      a.path.should == 'sugarce'
      a.host.should == 'localhost'
      a.user.should == 'admin'
      a.url.should  == 'http://localhost/sugarce/service/v4_1/rest.php'
    end
  end

  describe :run_request do
    it 'should send a request' do
      connection = @connection.instance

      response = connection.run_request('test', {'data'=>'test'})
      response.should_not be_nil
      expect { JSON.parse(response) }.to_not raise_error
    end
  end

  describe :create_session do
    it 'should log in to SugarCRM' do
      connection = @connection.instance

      connection.instance_variable_set(:@session_id, nil)
      connection.create_session do |session|
        connection.session_id.should_not be_nil
        connection.session_id.should_not be_empty
      end
    end

    it 'should not log in to SugarCRM and raise error' do
      subject = @connection.instance

      subject.password = 'letmein'
      subject.instance_variable_set(:@session_id, nil)
      expect {subject.create_session}.to raise_error('LoginError')
    end
  end

  describe :destroy_session do
    it 'should log out off SugarCRM' do
      subject = @connection.instance

      subject.instance_variable_set(:@session_id, nil)
      subject.create_session do |session|
        expect {subject.destroy_session}.to_not raise_error
        subject.session_id.should be_nil
      end
    end

    it 'should raise NoSessionError' do
      subject = @connection.instance

      subject.password = 'letmein'
      subject.instance_variable_set(:@session_id, nil)
      expect {subject.create_session}.to raise_error('LoginError')

      # Session hasn't been created !
      expect {subject.destroy_session}.to raise_error('NoSessionError')
    end
  end

  describe :get_available_modules do
    it 'should get all available modules for current user' do
      result = @connection.instance
      subject = result.get_available_modules
      subject.should include('Home')
      subject.should include('Accounts')
    end
  end

  describe :persist_session do
    it 'should renew session after session_interval' do
      subject = @connection.instance

      # Renew session after 3 seconds
      subject.session_interval = 0.05

      subject.create_session do |session|
        subject.run_request('test', {})
        old_session_id = subject.session_id.clone

        sleep 6

        subject.run_request('test', {})        
        new_session_id = subject.session_id

        old_session_id.should_not == new_session_id
      end
    end

    it "should not renew session if the interval haven't met" do
      subject = @connection.instance

      subject.session_interval = 4

      subject.create_session do |session|
        subject.run_request('test', {})
        old_session_id = subject.session_id.clone

        sleep 6

        subject.run_request('test', {})
        new_session_id = subject.session_id

        old_session_id.should == new_session_id
      end
    end
  end
end