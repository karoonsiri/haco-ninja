# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'haco_ninja/version'

Gem::Specification.new do |spec|
  spec.name          = "haco_ninja"
  spec.version       = HacoNinja::VERSION
  spec.authors       = ["Swiftlet Co., Ltd."]
  spec.email         = ["karuns@swiftlet.co.th"]
  spec.description   = %q{Haco Group CRM Ninja}
  spec.summary       = %q{Haco Group CRM Ninja}
  spec.homepage      = "http://www.swiftlet.co.th"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  # Development Dependencies
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rest-client"
  spec.add_development_dependency "json"
  spec.add_development_dependency "activesupport"

  # Production Dependencies
  spec.add_dependency "rest-client"
  spec.add_dependency "json"
  spec.add_dependency "activesupport"

end
