
module HacoNinja

  class Configuration
    attr_accessor :sugar_host, :sugar_path, :sugar_user, :sugar_password
    attr_accessor :sugar_session_interval, :sugar_max_retry, :sugar_cache

    def initialize
      self.sugar_host = 'localhost'
      self.sugar_path = 'sugarcrm'
      self.sugar_user = 'admin'
      self.sugar_password = ''
      self.sugar_session_interval = 4
      self.sugar_max_retry = 3
      self.sugar_cache = false
    end

    def self.configuration
      @configuration ||= Configuration.new
    end

    def self.configure
      yield(configuration) if block_given?
    end

    def reset
      @configuration = Configuration.new
    end
  end
end
