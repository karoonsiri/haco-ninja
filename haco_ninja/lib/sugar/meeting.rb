require 'sugar/base'

module Sugar
  class Meeting < Base;

    @link_fields = {
        'contacts'    => ['id','full_name','account_name','email1', 'phone_work'],
        'users'       => ['id', 'full_name', 'user_name', 'title', 'department','email1','phone_work'],
        'accounts'    => ['id','name','billing_address_city', 'billing_address_country', 'phone_office'],
        'leads'       => ['id', 'full_name' , 'refered_by','lead_source', 'lead_source_description' ,'phone_work', 'email1', 'assigned_user_name'],
        'opportunity' => ['id', 'name', 'sales_stage', 'date_closed', 'amount', 'account_name', 'assigned_user_name'],
        'case'        => ['id', 'case_number' , 'name', 'status', 'priority', 'date_entered','assigned_user_name'],
        'notes'       => ['id', 'name', 'contact_name','status' ,'date_modified','date_entered', 'assigned_user_name']
      }
  end
end
