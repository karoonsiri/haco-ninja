require 'sugar/base'

module Sugar
  class Case < Base;
    @link_fields = {
      'tasks'     => ['id', 'name','status','contact_name','date_due', 'assigned_user_name'],
      'notes'     => ['id', 'name', 'contact_name','status' ,'date_modified','date_entered', 'assigned_user_name'],
      'meetings'  => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'emails'    => ['id', 'name', 'status', 'contact_name', 'date_entered', 'date_modified','assigned_user_name' ],
      'documents' => ['id', 'document_name','filename','category_id', 'status_id' ,'active_date'],
      'calls'     => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'bugs'      => ['id', 'bug_number', 'name', 'status', 'type', 'priority', 'assigned_user_name'],
      'contacts'  => ['id', 'full_name','account_name','email1', 'phone_work'],
      'accounts'  => ['id', 'name'],
      'project'   => ['id', 'name','assigned_user_name','estimated_start_date','estimated_end_date']
    }

  end
end
