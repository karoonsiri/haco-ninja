require 'sugar/base'

module Sugar
  class Lead < Base;
     @link_fields = {
         'email_addresses_primary' => ['id', 'email_address'],
         'email_addresses'         => ['id', 'email_address'],
         'reports_to_link'         => ['id', 'full_name','user_name', 'title', 'department','email1'],
         'reportees'               => ['id', 'full_name','user_name', 'title', 'department','email1'],
         'contacts'                => ['id', 'full_name','primary_address_city', 'primary_address_state', 'email1' , 'phone_work'],
         'accounts'                => ['id','name','billing_address_city', 'billing_address_country', 'phone_office'],
         'contact'                 => ['id', 'full_name','primary_address_city', 'primary_address_state', 'email1' , 'phone_work'],
         'opportunity'             => ['id', 'name', 'sales_stage', 'date_closed', 'amount', 'account_name', 'assigned_user_name'],
         'campaign_leads'          => ['id', 'name'],
         'tasks'                   => ['id', 'name','status','contact_name','date_due', 'assigned_user_name']  ,
         'notes'                   => ['id', 'name', 'contact_name','status' ,'date_modified','date_entered', 'assigned_user_name'],
         'meetings'                => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
         'calls'                   => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
         'oldmeetings'             => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
         'oldcalls'                => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
         'emails'                  => ['id', 'name', 'from_addr_name', 'to_addrs_names', 'description', 'date_sent', 'status', 'type'],
         'campaigns'               => ['id', 'name' ,'status', 'start_date', 'end_date', 'assigned_user_name'],
         'prospect_lists'          => ['id', 'name', 'list_type', 'description', 'date_entered']
   }

  end
end
