require 'sugar/base'

module Sugar
  class User < Base;

    @link_fields = {
      'calls'                   => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'meetings'                => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'contacts_sync'           => ['id', 'full_name'],
      'email_addresses_primary' => ['id', 'email_address'],
      'email_addresses'         => ['id', 'email_address'],
      'reports_to_link'         => ['id', 'full_name','user_name', 'title', 'department','email1'],
      'reportees'               => ['id', 'full_name','user_name', 'title', 'department','email1'],
      'aclroles'                => ['id', 'name' , 'description'],
      'prospect_lists'          => ['id', 'name', 'list_type', 'description', 'date_entered'],
      'emails_users'            => ['id', 'email_address'],
      'holidays'                => ['id', 'name'],
      'eapm'                    => ['id','name','description', 'password', 'url', 'application', 'api_data',
                                     'consumer_key','consumer_secret','oauth_token','oauth_secret', 'validated', 'note'],
      'oauth_tokens'            => ['id', 'oauth_token','oauth_secret'],
      'project_resource'        => ['id', 'name']
    }

  end
end
