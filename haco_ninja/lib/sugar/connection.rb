require 'haco_ninja/configuration'
require 'singleton'
require 'json'
require 'rest_client'
require 'digest/md5'

module Sugar
  # === Error code definition
  #   LoginError -- Happens when user failed to log in
  #   ConnectionError -- Happens when the host cannot be reached
  #   DestroySessionError -- Happens when the class cannot destroy user session
  #   NoSessionError -- Happens when user session has expired or invalid user session given

  class Connection
    # Make this connection class singleton
    include Singleton

    # === Class attributes
    #   accessor 
    #     :host -- Hostname of SugarCRM
    #     :path -- Path to SugarCRM application
    #     :user -- User used to log in to SugarCRM
    #     :session_interval -- A period of time before session renewal
    #   writer   
    #     :password -- Password of user used to log in to SugarCRM
    #     :max_retry -- Maximum number of connection retries before raising an error
    #   reader   
    #     :url  -- Complete URL of SugarCRM REST service
    #     :session_id -- Current logged in user's session ID
    attr_accessor :host, :path, :user, :max_retry, :session_interval
    attr_reader   :url, :session_id
    attr_writer   :password, :max_retry

    def initialize
      @host     = HacoNinja::Configuration.configuration.sugar_host
      @path     = HacoNinja::Configuration.configuration.sugar_path
      @user     = HacoNinja::Configuration.configuration.sugar_user
      @password = HacoNinja::Configuration.configuration.sugar_password

      @url  = 'http://' + "#{@host}/#{@path}/service/v4_1/rest.php".gsub(/\/\/+/,'/')

      init_resource

      @resource = RestClient::Resource.new(@url)
    end

    def host=(value)
      @host = value.gsub(/^\/*([^\/]+)\/*$/,'\1') rescue 'localhost'
      @url  = 'http://' + "#{@host}/#{@path}/service/v4_1/rest.php".gsub(/\/\/+/,'/')
      @resource = RestClient::Resource.new(@url)
    end

    def path=(value)
      @path = value.gsub(/^(\/*)([^\/]+\/*[^\/]+)(\/*)$/,'\2') rescue ''
      @url  = 'http://' + "#{@host}/#{@path}/service/v4_1/rest.php".gsub(/\/\/+/,'/')
      @resource = RestClient::Resource.new(@url)
    end

    def run_request(method, param)
      post_param = {
        'method' => method,
        'input_type' => 'JSON',
        'response_type' => 'JSON',
        'rest_data' => param.to_json
      }

      # For (@session_interval) minutes interval
      persist_session if method != 'login' && method != 'logout'

      @resource.post(post_param, @request_headers)
    end

    def create_session
      unless @session_id.nil? || @session_id.empty? || Time.now > (@session_time + (60*@session_interval))
        yield @session_id if block_given?
        return
      end
      user = try_connection

      if user.has_key?('id')
        @session_id = user['id']
        @connection_status = :fine
        @session_time = Time.now
        yield user if block_given?
      else
        @session_id = nil
        raise 'ConnectionError' if @connection_status == :error
        raise 'LoginError'
      end
    
    end

    def destroy_session
      check_session
      params = {
        'session' => @session_id
      }
      begin
        response = JSON.parse(run_request('logout', params))
        raise 'DestroySessionError'
      rescue
        # Logout successful if exception is raised
        @session_id.clear
        @session_id = nil
      end
    end

    def check_session
      raise 'NoSessionError' if @session_id.nil? || @session_id.empty?
    end

    def get_available_modules
      persist_session if @session_id.nil? || @session_id.empty?

      param = {
        'session' => @session_id,
        'filter'  => 'all',
      }

      # pp run_request('get_available_modules', param)
      JSON.parse(run_request('get_available_modules', param))['modules'].inject([]) do |result, name|
        result << name['module_key']
      end
    end

    private

    def persist_session
      create_session {} if @session_id.nil? || @session_id.empty? || Time.now > (@session_time + (60*@session_interval))
      @session_time = Time.now
    end

    def try_connection
      params = {
        'user_auth' => {
          'user_name' => @user,
          'password' => Digest::MD5.hexdigest(@password),
          'version' => '1',
        },
        'application_name' => 'HacoNinja',
        'name_value_list' => []
      }

      @max_retry.times do |reconnect|
        begin
          @connection_status = :fine
          return JSON.parse(run_request('login',params))
        rescue
          puts 'Waiting 5 seconds to reconnect...'
          sleep 5
          next
        end
      end

      @connection_status = :error
      {}
    end

    def init_resource
      @request_headers = {
        'User-Agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0',
      }
      @max_retry = 3
      @connection_status = :fine
      @session_time = Time.now
      @session_interval = 15
    end

  end
end
