require 'sugar/base'

module Sugar
  class Prospect < Base;

    @link_fields = {
      'email_addresses_primary' => ['id', 'email_address'],
      'email_addresses'         => ['id', 'email_address'],
      'campaigns'     => ['id', 'name' ,'status', 'start_date', 'end_date', 'assigned_user_name'],
      'prospect_lists'=> ['id', 'name', 'list_type', 'description', 'date_entered'],
      'meetings'      => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'calls'         => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'emails'        => ['id', 'name', 'from_addr_name', 'to_addrs_names', 'description', 'date_sent', 'status', 'type'],
      'notes'         => ['id', 'name', 'contact_name','status' ,'date_modified','date_entered', 'assigned_user_name'],
      'tasks'         => ['id', 'name', 'status', 'contact_name', 'date_due',   'assigned_user_name']
    }

  end
end
