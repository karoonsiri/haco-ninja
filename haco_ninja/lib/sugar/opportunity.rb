require 'sugar/base'

module Sugar
  class Opportunity < Base;

    @link_fields = {
      'campaign_opportunities' => ['id', 'name'],
      'accounts'               => ['id','name','billing_address_city', 'billing_address_country', 'phone_office'],
      'contacts'               => ['id', 'full_name','account_name','primary_address_city', 'primary_address_state', 'email1' , 'phone_work'],
      'tasks'                  => ['id', 'name','status','contact_name','date_due', 'assigned_user_name'],
      'notes'                  => ['id', 'name', 'contact_name','status' ,'date_modified','date_entered', 'assigned_user_name'],
      'meetings'               => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'calls'                  => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'emails'                 => ['id', 'name', 'from_addr_name', 'to_addrs_names', 'description', 'date_sent', 'status', 'type'],
      'documents'              => ['id', 'document_name','filename','category_id', 'status_id' ,'active_date'],
      'project'                => ['id', 'name','assigned_user_name','estimated_start_date','estimated_end_date'],
      'leads'                  => ['id', 'full_name' ,'refered_by','lead_source', 'lead_source_description' ,'phone_work', 'email1', 'assigned_user_name'],
      'campaigns'              => ['id', 'name' ,'status', 'start_date', 'end_date', 'assigned_user_name'],
      'campaign_link'          => ['id', 'name' ,'status', 'start_date', 'end_date', 'assigned_user_name'],
      'currencies'             => ['id', 'name' , 'symbol', 'status', 'conversion_rate' ,'iso4217']
      }

  end
end
