require 'sugar/base'

module Sugar
  class Campaign < Base;
    @link_fields = {
      'prospectlists' => ['id','name','description','list_type'],
      'emailmarketing' => ['id','name','date_start','status','template_id','template_name'],
      'queueitems' => [],
      'log_entries' => [],
      'tracked_urls' => ['id','tracker_name', 'tracker_url' ,'tracker_key'],
      'leads' => ['id', 'full_name' ,'refered_by','lead_source', 'lead_source_description' ,'phone_work', 'email1', 'assigned_user_name'],
      'opportunities' => ['id', 'name', 'sales_stage', 'date_closed', 'amount', 'account_name', 'assigned_user_name'],
      'contacts' => ['id','full_name','account_name','email1', 'phone_work'],
      'accounts' => ['id','name','billing_address_city', 'billing_address_country', 'phone_office']
    }

  end
end
