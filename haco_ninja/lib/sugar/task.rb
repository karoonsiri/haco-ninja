require 'sugar/base'

module Sugar
  class Task < Base;

    @link_fields = {
      'contacts'       => ['id', 'full_name','account_name','primary_address_city', 'primary_address_state', 'email1' , 'phone_work'],
      'accounts'       => ['id','name','billing_address_city', 'billing_address_country', 'phone_office' , 'email1', 'assigned_user_name'],
      'opportunities'  => ['id', 'name', 'sales_stage', 'date_closed', 'amount', 'account_name', 'assigned_user_name'],
      'case'           => ['id', 'case_number' , 'name', 'status', 'priority', 'date_entered','assigned_user_name'],
      'bugs'           => ['id', 'bug_number', 'name', 'status', 'type', 'priority', 'assigned_user_name'],
      'leads'          => ['id', 'full_name' ,'refered_by','lead_source', 'lead_source_description' ,'phone_work', 'email1', 'assigned_user_name'],
      'project'        => ['id', 'name','assigned_user_name','estimated_start_date','estimated_end_date'],
      'projecttask'    => ['id', 'name', 'status', 'description','date_start','time_start','date_finish','time_finish' ,'assigned_user_name'],
      'notes'          => ['id', 'name', 'contact_name','status' ,'date_modified','date_entered', 'assigned_user_name'],
      'contact_parent' => ['id', 'full_name']
    }

  end
end
