require 'haco_ninja'

module Sugar
  class Project < Base

    @link_fields = {
      'accounts'      => ['id','name','billing_address_city', 'billing_address_country', 'phone_office'],
      'quotes'        => [],
      'contacts'      => ['id', 'full_name','account_name','primary_address_city', 'primary_address_state', 'email1' , 'phone_work'],
      'opportunities' => ['id', 'name', 'sales_stage', 'date_closed', 'amount', 'account_name', 'assigned_user_name'],
      'notes'         => ['id', 'name', 'contact_name','status' ,'date_modified','date_entered', 'assigned_user_name'],
      'tasks'         => ['id', 'name', 'status', 'contact_name', 'date_due',   'assigned_user_name'],
      'meetings'      => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'calls'         => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'emails'        => ['id', 'name', 'from_addr_name', 'to_addrs_names', 'description', 'date_sent', 'status', 'type'],
      'projecttask'   => ['id', 'name', 'status', 'description','date_start','time_start','date_finish','time_finish' ,'assigned_user_name'],
      'cases'         => ['id', 'case_number' , 'name', 'account_name' ,'status', 'priority', 'date_entered','assigned_user_name'],
      'bugs'          => ['id', 'bug_number', 'name', 'status', 'type', 'priority', 'assigned_user_name'],
      'products'      => []
    }

    def self.module_name
      'Project'
    end
  end
end
