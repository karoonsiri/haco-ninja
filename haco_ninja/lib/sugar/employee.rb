require 'sugar/base'

module Sugar
  class Employee < Base;

   @link_fields = {
      'calls'           => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'meetings'        => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'contacts_sync'   => ['id', 'full_name'],
      'reports_to_link' => ['id', 'full_name','user_name', 'title', 'department','email1'],
      'reportees'       => ['id', 'full_name', 'user_name', 'title', 'department','email1'],
      'email_addresses' => ['id', 'email_address'],
      'email_addresses_primary' => ['id', 'email_address'],
      'aclroles'        => ['id', 'name' , 'description'],
      'prospect_lists'  => ['id', 'name', 'list_type', 'description', 'date_entered'],
      'emails_users'    => ['id', 'email_address'],
      'holidays'        => ['id', 'name'],
      'eapm'            => [],
      'oauth_tokens'    => [],
      'project_resource' => ['id', 'name']
    }

  end
end
