# encoding: UTF-8
require 'haco_ninja/configuration'
require 'sugar/connection'
require 'json'
require 'rest_client'
require 'digest/md5'
require 'pp'

module Sugar

  # === Sugar::Base
  #  A mother class for SugarCRM records driver.
  #  Needs to be extended with the exact same name to SugarCRM's module names
  #  with camelcase and in singular form to be usable.
  #  For example:
  #    class Contact < Sugar::Base
  #  or
  #    class Lead < Sugar::Base
  class Base

    # === Class attributes
    #   accessor
    #     @[module_fields] -- Fields of the module, from SugarCRM application
    #   writer
    #     - none -
    #   reader
    #     :module_name -- Module name according to SugarCRM
    #     :module_fields -- Available fields of the module

    # === Error Definition
    #   SetRelationshipError - When trying to associate a bean but owner bean is not saved

    attr_reader :module_name, :module_fields

    def initialize(*args)
      @connection = Sugar::Connection.instance
      @connection.create_session
      @module_name = self.class.module_name
      @is_cache = HacoNinja::Configuration.configuration.sugar_cache

      args = args.first

      init_resource(args)
    end

    def self.create(*args)
      args = args.first

      candidate = new(args)
      candidate.save
      candidate
    end

    def save
      request_param = {
        'session' => @connection.session_id,
        'module_name' => @module_name,
        'name_value_list' => build_save_params,
      }

      response = JSON.parse(@connection.run_request('set_entry', request_param))
      @id = response['id']
      response
    end

    def associate!(link)
      raise "SetRelationshipError" if @id.nil?

      link_name = link.module_name.underscore
      
      param = {
        'session' => @connection.session_id,
        'module_name' => @module_name,
        'module_id' => @id,
        'link_field_name' => link_name,
        'related_ids' => [link.id],
        'name_value_list' => [],
        'delete' => '0',
      }

      JSON.parse(@connection.run_request('set_relationship', param))
      self.class.find(@id)
    end

    def disassociate!(link_name, id)
      raise "SetRelationshipError" if @id.nil?

      param = {
        'session' => @connection.session_id,
        'module_name' => @module_name,
        'module_id' => @id,
        'link_field_name' => link_name,
        'related_ids' => [id],
        'name_value_list' => [],
        'delete' => '1',
      }

      JSON.parse(@connection.run_request('set_relationship', param))
      self.class.find(@id)
    end

    def update_attributes(*args)
      args = args.first ||= {}

      args.keys.each do |param|
        instance_variable_set("@#{param}", args[param])
      end

      save
    end

    def delete
      delete_param = {
        'session' => @connection.session_id,
        'module_name' => module_name,
        'name_value_list' => [
          { 'name' => 'id', 'value' => @id },
          { 'name' => 'deleted', 'value' => '1' },
        ],
      }

      return nil if @id.nil?

      JSON.parse(@connection.run_request('set_entry', delete_param))
    end

    def self.find(id)
      connection = Sugar::Connection.instance
      connection.create_session
      module_name = self.module_name

      params = {
        'session' => connection.session_id,
        'module_name' => module_name,
        'query' => "#{module_name.underscore}.id = '#{id}'",
        'order_by' => '',
        'offset' => '0',
        'select_fields' => get_module_fields,
        'link_name_to_fields_array' => link_fields,
        'max_results' => '999999',
        'deleted' => '0',
      }

      response = JSON.parse(connection.run_request('get_entry_list', params))
      user = clean_entry_list(response, [])[0]

      return nil if user.nil?

      new(user)
    end

    def self.find_where(*args)
      connection = Sugar::Connection.instance
      connection.create_session
      module_name = self.module_name

      deleted = '0'
      args.each { |arg| deleted = '1' if arg =~ /.+\.deleted.*=.*1.*/ }
      args = args.join(' AND ')

      params = {
        'session' => connection.session_id,
        'module_name' => module_name,
        'query' => args,
        'order_by' => '',
        'offset' => '0',
        'select_fields' => get_module_fields,
        'link_name_to_fields_array' => link_fields,
        'max_results' => '999999',
        'deleted' => deleted,
      }

      response = JSON.parse(connection.run_request('get_entry_list', params) || '{}')

      return [] if response.empty?

      users = clean_entry_list(response, [])

      final_result = users.map do |user|
        new(user)
      end
    end

    def self.all
      find_where('')
    end

    def self.get_module_fields
      connection = Sugar::Connection.instance
      connection.create_session
      params = {
        'session' => connection.session_id,
        'module_name' => module_name,
        'fields' => [],
      }

      response = JSON.parse(connection.run_request('get_module_fields', params))
      module_fields = response['module_fields'].map do |data|
        data[0]
      end
    end

    def self.module_name
      to_s.gsub(/.+::/, '').pluralize
    end

    def self.link_fields
      @link_fields ||= {}
      @link_fields.map do |key, value|
        {
          'name' => key,
          'value' => value,
        }
      end
    end


    # Caching options method
    def self.cached
      @cached ||= false
    end

    def self.cached=(value)
      @cached = value
    end

    def self.module_fields_cache
      @module_fields ||= []
    end

    def self.module_fields_cache=(value)
      @module_fields = value
    end

    # End caching options method

    # Private starts here

    private

    def init_resource(*args)
      args = args.first
      set_module_fields(args)
    end

    def set_module_fields(*args)
      args = args.first || {}

      params = {
        'session' => @connection.session_id,
        'module_name' => module_name,
        'fields' => [],
      }

      response = {}

      unless self.class.cached
        response = JSON.parse(@connection.run_request('get_module_fields', params))      

        @module_fields = response['module_fields'].map { |(key, value)| key }

        if @is_cache
          self.class.module_fields_cache = @module_fields
          self.class.cached = true
        end
      else
        @module_fields = self.class.module_fields_cache
      end

      @module_fields.each do |(key,value)|
        (class << self; self; end).send(:attr_accessor, key.to_sym)
        instance_variable_set("@#{key}", (args || {key => nil})[key])
        args.delete(key) rescue nil
      end

      unless args.empty?
        args.each do |key, value|
          (class << self; self; end).send(:attr_accessor, key.to_sym)
          instance_variable_set("@#{key}", value)
          @module_fields << key
        end
      end

    end

    def build_save_params
      name_value_list = @module_fields.map do |field|
        {
          'name' => field,
          'value' => instance_variable_get("@#{field}") || ''
        }
      end
    end

    def self.clean_entry_list(response, link_fields_request = [])
      # pp response
      return nil if response['result_count'] == '0'

      final_result = response['entry_list'].map.with_index do |record, index|
        single = {}
        record['name_value_list'].each do |key, value_hash|
          single[value_hash['name']] = value_hash['value']
        end

        single['related_fields'] = {}
        if response['relationship_list'].length > 0
          response['relationship_list'][index]['link_list'].each do |link_list|
            single['related_fields'][link_list['name']] = link_list['records'].map do |item|
              to_insert = {}
              item['link_value'].each do |attribute|
                to_insert[attribute[0]] = attribute[1]['value']
              end
              to_insert
            end
          end
        end

        single
      end
    end

  end
end
