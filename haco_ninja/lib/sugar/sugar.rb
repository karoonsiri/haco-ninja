require 'haco_ninja/configuration'
require 'sugar/connection'
require 'active_support/inflector'
require 'sugar/base'
require 'sugar/account'
require 'sugar/call'
require 'sugar/campaign'
require 'sugar/case'
require 'sugar/contact'
require 'sugar/employee'
require 'sugar/lead'
require 'sugar/meeting'
require 'sugar/opportunity'
require 'sugar/project'
require 'sugar/prospect'
require 'sugar/prospect_list'
require 'sugar/task'
require 'sugar/user'

require 'sugar/aos_contracts'
require 'sugar/aos_quotes'
require 'sugar/aos_products'
require 'sugar/aos_products_quotes'

HacoNinja::Configuration.configure do |config|
  config.sugar_host = 'localhost'
  config.sugar_path = 'sugarcrm'
  config.sugar_user = 'admin'
  config.sugar_password = ''
  config.sugar_session_interval = 4
  config.sugar_max_retry = 3
end
