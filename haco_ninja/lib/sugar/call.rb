require 'sugar/base'

module Sugar
  class Call < Base;
    @link_fields = {
      'opportunities' => ['id', 'name'],
      'leads' => ['id', 'full_name' , 'account_name','email1', 'phone_work'],
      'project' => ['id','name'],
      'case' => ['id', 'name'],
      'accounts' => ['id','name'],
      'contacts' => ['id','full_name','account_name','email1', 'phone_work'],
      'users' => ['id', 'full_name', 'user_name', 'email1' , 'phone_work'],
      'notes' => ['id', 'name', 'contact_name','date_modified','date_entered', 'assigned_user_name']
    }

  end
end
