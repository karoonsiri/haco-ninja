require 'sugar/base'

module Sugar
  class AOS_Products_Quote < Base;
    @link_fields = {
      'aos_products'       => ['id', 'name','description','maincode','part_number','category', 'type', 'cost',
                               'currency_id', 'price', 'url' , 'contact' , 'product_image']
    }
  end
end
