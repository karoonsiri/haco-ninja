require 'sugar/base'

module Sugar
  class ProspectList < Base;
    @link_fields = {
      'prospects'       => ['id', 'name','title', 'email1', 'phone_work'],
      'contacts'        => ['id', 'full_name','account_name','primary_address_city', 'primary_address_state', 'email1' , 'phone_work'],
      'leads'           => ['id', 'full_name' ,'refered_by','lead_source', 'lead_source_description' ,'phone_work', 'email1', 'assigned_user_name'],
      'accounts'        => ['id','name','billing_address_city', 'billing_address_country', 'phone_office' , 'email1', 'assigned_user_name'],
      'campaigns'       => ['id', 'name' ,'status', 'start_date', 'end_date', 'assigned_user_name'],
      'users'           => ['id', 'full_name', 'user_name', 'email1' , 'phone_work'],
      'email_marketing' => ['id', 'name', 'from_name' , 'from_addr', 'reply_to_name', 'reply_to_addr', 'date_start', 'status']
    }
  end
end
