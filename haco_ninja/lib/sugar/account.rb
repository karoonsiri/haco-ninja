require 'sugar/base'

module Sugar
  class Account < Base

    @link_fields = {
      'email_addresses' => ['id', 'email_address'],
      'email_addresses_primary' => ['id', 'email_address'],
      'contacts'        => ['id', 'full_name','primary_address_city', 'primary_address_state', 'email1' , 'phone_work'],
      'tasks'           => ['id', 'name','status','contact_name','date_due', 'assigned_user_name'],
      'members'         => ['id', 'name'],
      'member_of'       => ['id', 'name'],
      'cases'           => ['id', 'case_number' , 'name', 'status', 'priority', 'date_entered','assigned_user_name'],
      'notes'           => ['id', 'name', 'contact_name','status' ,'date_modified','date_entered', 'assigned_user_name'],
      'opportunities'   => ['id', 'name', 'sales_stage', 'date_closed', 'amount', 'account_name', 'assigned_user_name'],
      'project'         => ['id','name'],
      'leads'           => ['id', 'full_name' , 'account_name','email1', 'phone_work'],
      'campaigns'       => ['id', 'name' ,'status', 'start_date', 'end_date', 'assigned_user_name'],
      'campaign_accounts' => ['id', 'name'],
      'prospect_lists'  => ['id', 'name', 'list_type', 'description', 'date_entered'],
      'meetings'        => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'calls'           => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'documents'       => ['id', 'document_name','filename','category_id', 'status_id','active_date'],
      'bugs'            => ['id', 'bug_number', 'name', 'status', 'type', 'priority', 'assigned_user_name']
    }

  end
end
