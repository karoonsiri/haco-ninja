require 'sugar/base'

module Sugar
  class AOS_Quote < Base;
    @link_fields = {
      'aos_quotes_project'       => ['id', 'name','assigned_user_name','estimated_start_date','estimated_end_date'],
      'aos_quotes_aos_invoices'  => ['id', 'name', 'number','total_amount','status', 'assigned_user_name'],
      'aos_quotes_aos_contracts' => ['id', 'name', 'description', 'start_date', 'end_date', 'status' ,'currency_id',
                                      'total_contract_value', 'contract_account_id','contract_account', 'assigned_user_name'],
      'aos_products_quotes'      => ['id', 'name', 'description', 'number', 'product_qty', 'product_cost_price',
                                     'product_list_price', 'product_discount', 'product_discount_amount', 'discount',
                                     'product_unit_price', 'vat_amt', 'product_total_price', 'vat', 'parent_name',
                                     'parent_type', 'parent_id', 'product_id', 'group_id']
    }
  end
end
