require 'sugar/base'

module Sugar
  class Contact < Base;

    @link_fields = {
      'email_addresses' => ['id', 'email_address'],
      'email_addresses_primary' => ['id', 'email_address'],
      'accounts'        => ['id','name','billing_address_city', 'billing_address_country', 'phone_office'],
      'opportunities'   => ['id', 'name', 'sales_stage', 'date_closed', 'amount', 'account_name', 'assigned_user_name'],
      'bugs'            => ['id', 'bug_number', 'name', 'status', 'type', 'priority', 'assigned_user_name'],
      'calls'           => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'cases'           => ['id', 'case_number' , 'name', 'status', 'priority', 'date_entered','assigned_user_name'],
      'direct_reports'  => ['id', 'full_name', 'account_name', 'email1', 'phone_work'],
      'emails'          => ['id', 'email_address'],
      'documents'       => ['id', 'document_name','filename','category_id','status_id' ,'active_date'],
      'leads'           => ['id', 'full_name' ,'refered_by','lead_source', 'lead_source_description' ,'phone_work', 'email1', 'assigned_user_name'],
      'meetings'        => ['id', 'name', 'status', 'contact_name', 'date_start', 'assigned_user_name'],
      'notes'           => ['id', 'name', 'contact_name','status' ,'date_modified','date_entered', 'assigned_user_name'],
      'project'         => ['id', 'name','assigned_user_name','estimated_start_date','estimated_end_date'],
      'project_resource' => ['id', 'name'],
      'tasks'           => ['id', 'name','status','contact_name','date_due', 'assigned_user_name'],
      'tasks_parent'    => ['id', 'name','status','contact_name','date_due', 'assigned_user_name'],
      'user_sync'       => ['id', 'full_name'],
      'campaigns'       => ['id', 'name' ,'status', 'start_date', 'end_date', 'assigned_user_name'],
      'campaign_accounts' => ['id', 'name'],
      'prospect_lists'  => ['id', 'name', 'list_type', 'description', 'date_entered']
    }

  end
end
